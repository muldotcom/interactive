class AbsentModel {
  String? status;
  Data? data;
  String? apiVersionCode;

  AbsentModel({this.status, this.data, this.apiVersionCode});

  AbsentModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    apiVersionCode = json['api_version_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['api_version_code'] = this.apiVersionCode;
    return data;
  }
}

class Data {
  String? id;
  String? key;
  String? checkinDate;
  String? checkinTime;
  String? lat;
  String? long;

  Data({this.id,this.key, this.checkinDate, this.checkinTime, this.lat, this.long});

  Data.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    checkinDate = json['checkin_date'];
    checkinTime = json['checkin_time'];
    lat = json['lat'];
    long = json['long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['checkin_date'] = this.checkinDate;
    data['checkin_time'] = this.checkinTime;
    data['lat'] = this.lat;
    data['long'] = this.long;
    return data;
  }
}