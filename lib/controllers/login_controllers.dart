import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import 'package:interactivetest/models/AbsentModel.dart';
import 'package:interactivetest/view/dashboard_page.dart';

import '../utils/config.dart';
import '../utils/my_colors.dart';

class UserController extends GetxController {
  final Dio dio = Dio(
    BaseOptions(
      baseUrl: Config.baseURL + Config.prefixUrl,
      connectTimeout: 50000,
      receiveTimeout: 50000,
    ),
  );


  Future<Response> get({
    required String endpoint,
    required String contentType,
  }) async {
    var response = await dio.get(
      endpoint,
      options: Options(
        headers: {
          "Content-type": contentType,
        },
      ),
    );
    return response;
  }


  Future<Response> post(
      {required String endpoint,
        required String contentType,
        dynamic body}) async {
    print("endpoint : ${endpoint}");
    print("params : ${body}");
    var response = await dio.post(endpoint,
        options: Options(
          headers: {
            "Content-type": contentType,
          },
        ),
        data: body);
    return response;
  }
  RxBool isLoading = false.obs;
  RxBool isAbsent = false.obs;
   loginUser(String username,String password) async {
     Get.offAll(() => DashboardPage());
     return;
     print("loginUser");
     print(username);
     print(password);
     print("loginUser");
     if(username == "1111" && password == "pass"){
      Get.offAll(() => DashboardPage());
    }
    else{
      Get.snackbar(
        "Login Gagal",
        "Username atau Password anda Salah",
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
    }
   }

  Future<AbsentModel?> addAbsent({
    required String apikey,
    required String checkin_date,
    required String checkin_time,
    required String lat,
    required String long
  }) async {
    isLoading.value = true;
    // DateTime now = new DateTime.now();
    // DateTime date = new DateTime(now.year, now.month, now.day);
    // var tanggal = "${date.year}:${date.month}:${date.day}";
    // var jam = "${now.hour.toString().padLeft(2,'0')}:${now.minute.toString().padLeft(2,'0')}:${now.second.toString().padLeft(2,'0')}";

    var params2 = {
      'apikey': apikey,
      'checkin_date': checkin_date,
      'checkin_time': checkin_time,
      'lat': lat,
      'long': long,
    };

    var params = FormData.fromMap({
      'apikey': 'muliarifai',
      'checkin_date': checkin_date,
      'checkin_time': checkin_time,
      'lat': lat,
      'long': long,
    });

    print(params2);
    try {
      Response response = await post(
          endpoint: Config.endpoint_absent,
          body: params,
          contentType: "application/json");
      print(response.data);
      Get.snackbar(
        "Absen Sukses",
        response.data.toString(),
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
      isLoading.value = false;
      AbsentModel absentModel = AbsentModel.fromJson(response.data);
      return absentModel;
    } on DioError catch (e) {
      print(e.message);
      Get.snackbar(
        "Users Create Failed",
        e.message,
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
      isLoading.value = false;
      return null;
    }
  }


  Future<bool> addAbsentSync({
    required String apikey,
    required String checkin_date,
    required String checkin_time,
    required String lat,
    required String long
  }) async {
    isLoading.value = true;
    // DateTime now = new DateTime.now();
    // DateTime date = new DateTime(now.year, now.month, now.day);
    // var tanggal = "${date.year}:${date.month}:${date.day}";
    // var jam = "${now.hour.toString().padLeft(2,'0')}:${now.minute.toString().padLeft(2,'0')}:${now.second.toString().padLeft(2,'0')}";

    var params2 = {
      'apikey': apikey,
      'checkin_date': checkin_date,
      'checkin_time': checkin_time,
      'lat': lat,
      'long': long,
    };

    var params = FormData.fromMap({
      'apikey': 'muliarifai',
      'checkin_date': checkin_date,
      'checkin_time': checkin_time,
      'lat': lat,
      'long': long,
    });

    print(params2);
    try {
      Response response = await post(
          endpoint: Config.endpoint_absent,
          body: params,
          contentType: "application/json");
      print(response.data);
      Get.snackbar(
        "Absen Sukses",
        response.data.toString(),
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
      isLoading.value = false;
      AbsentModel absentModel = AbsentModel.fromJson(response.data);
      await Future.delayed(Duration(seconds: 5));
      print("await");
      return true;
    } on DioError catch (e) {
      Get.snackbar(
        "Users Create Failed",
        e.message,
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
      isLoading.value = false;
      await Future.delayed(Duration(seconds: 5));
      print("await");
      return false;
    }
  }

  //
  // Future<UserResponse?> getUser() async {
  //   try {
  //     Response response = await get(
  //         endpoint: Config.get_user,
  //         contentType: "application/json");
  //     //print(response.data);
  //     UserResponse userResponse = UserResponse.fromJson(response.data);
  //     return userResponse;
  //   } on DioError catch (e) {
  //     return null;
  //   }
  // }
  //
  //
  // Future<UserDetailResponse?> getUserDetail({required int id}) async {
  //   try {
  //     Response response = await get(
  //         endpoint: Config.get_user+"/"+id.toString(),
  //         contentType: "application/json");
  //     UserDetailResponse userResponse = UserDetailResponse.fromJson(response.data);
  //     return userResponse;
  //   } on DioError catch (e) {
  //     return null;
  //   }
  // }
  //

  //



}