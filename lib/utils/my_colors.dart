import 'dart:ui';

class MyColors {

  static const Color primary = Color(0xFF00A0E9);
  static const Color primaryLight = Color(0xFFE5F6FD);
  static const Color primaryYellow = Color(0xFFFFF100);
  static const Color primaryYellowDark = Color(0xFFFFCF29);
  static const Color accent = Color(0xFFFF4081);
  static const Color accentDark = Color(0xFFDD2127);
  static const Color accentLight = Color(0xFFFF80AB);
  static const Color actionBar = Color(0xFF011C40);
  static const Color buttonGreen = Color(0xFF36D128);
  static const Color orangeYellow = Color(0xFFF5AB1C);
  static const Color dialogTextColor = Color(0xFF2552D9);
  static const Color calendarGreen = Color(0XFF36D128);
  static const Color calendarOrange = Color(0XFFF5AB1C);
  static const Color vividRad = Color(0xFFEB0D0D);

  static const Color accentHeading = Color(0XFF666666);
  static const Color accentColor = Color(0XFF999999);
  static const Color backgroundColor = Color(0XFFE2E2E2);

  static const Color blueTask = Color(0XFFE5F6FD);
  static const Color greenTask = Color(0XFFE8F1D4);
  static const Color lightGreenIcon = Color(0XFFF4F8EA);
  static const Color mediumTask = Color(0XFFFFF5D4);
  static const Color highTask = Color(0XFFFFE0E5);

  static const Color successText = Color(0XFF8EB72A);
  static const Color warningText = Color(0XFFFFCF29);
  static const Color errorText = Color(0XFFFF667C);


  //color picker
  static const Color color1 = Color(0xFFFF667C);
  static const Color color2 = Color(0xFFFFA3B0);
  static const Color color3 = Color(0xFFFFCF29);
  static const Color color4 = Color(0xFFFFE27F);
  static const Color color5 = Color(0xFFFFF100);
  static const Color color6 = Color(0xFFFFF766);
  static const Color color7 = Color(0xFF8EB72A);
  static const Color color8 = Color(0xFFBBD47F);
  static const Color color9 = Color(0xFF00A0E9);
  static const Color color10 = Color(0xFF66C6F2);





  //end




  static const Color borderColor = Color(0xFFE5E5E5);







  static const Color kakaotalkYellow = Color(0xFFffe812);
  static const Color naverGreen = Color(0xFF59C131);



  static const Color inputBorder = Color(0xFFDBDADA);
  static const Color primaryBackground = Color(0xFFD8DEF6);
  static const Color primaryBackgroundRed = Color(0xFFF6D8D8);
  static const Color divider = Color(0xFFD3DAEB);
  static const Color selectedBottomBar = Color(0xFF2f80ed);
  static const Color unselectedBottomBar = Color(0xFF878787);
  static const Color textColor = Color(0xFF878787);
  static const Color notificationsBackground = Color(0x1AA6AEB6);
  static const Color appBackground = Color(0xFFE5E5E5);
  static const Color headerOverlay = Color(0x99000000);





  //card color
  static const Color color_card_rab = Color(0xFF3185F6);
  static const Color color_card_project = Color(0xFF13CA25);
  static const Color color_green_dark = Color(0xFF27ae60);
  static const Color color_card_information = Color(0xFF17389B);
  static const Color color_card_point = Color(0xFFFF4248);
//



  static const Color grey_3 = Color(0xFFf7f7f7);
  static const Color grey_5 = Color(0xFFf2f2f2);
  static const Color grey_10 = Color(0xFFe6e6e6);
  static const Color grey_20 = Color(0xFFcccccc);
  static const Color grey_40 = Color(0xFF999999);
  static const Color grey_60 = Color(0xFF666666);
  static const Color grey_80 = Color(0xFF37474F);
  static const Color grey_90 = Color(0xFF263238);
  static const Color grey_95 = Color(0xFF1a1a1a);
  static const Color grey_100_ = Color(0xFF0d0d0d);


  static const Color green_10 = Color(0xFFF4F8EA);

  static const Color warningColor = Color(0xFFFFD481);

  static const Color greyTextColor = Color(0xFF9B9B9B);

  static const Color searchOverlay = Color(0x99000000);

  static const Color bgRandom = Color(0xFFFFECA9);
  static const Color bgMarket = Color(0xFF99D9F6);
  static const Color bgPoint = Color(0xFFCCECFB);
  static const Color bgSelected =  Color(0xFFFFE27F);
  static const Color bgDetailBingoCard = Color(0xFF33B3ED);
  static const Color toggleActiveColor = Color(0xFF4CD964);
  static const Color bgTimePicker = Color(0xFFF2FAFE);
  static const Color bgBadge = Color(0xFFFFA929);
  static const Color bgMinusBadge = Color(0xFFFDE7E7);
  static const Color black_80 = Color(0xFF333333);
  static const Color black_60 = Color(0xFF666666);
  static const Color black_40 = Color(0xFF999999);
  static const Color black_20 = Color(0xFFCCCCCC);
  static const Color black_10 = Color(0xFFE5E5E5);
  static const Color black_5 = Color(0xFFF2F2F2);
  static const Color blue_5 = Color(0xFFF2FAFE);
  static const Color blue_20 = Color(0xFFCCECFB);
  static const Color blue_10 = Color(0xFFE5F6FD);
  static const Color blue_40 = Color(0xFF99D9F6);
  static const Color blue_60 = Color(0xFF66C6F2);
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    if(hexString == ""){
      hexString = "#FFFFFF";
    }
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHexColors({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}