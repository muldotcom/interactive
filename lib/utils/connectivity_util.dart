import 'dart:async';
import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:is_first_run/is_first_run.dart';
class ConnectionUtil {
  //This creates the single instance by calling the `_internal` constructor specified below
  static final ConnectionUtil _singleton = new ConnectionUtil._internal();

  ConnectionUtil._internal();

  //This is what's used to retrieve the instance through the app
  static ConnectionUtil getInstance() => _singleton;

  //This tracks the current connection status
  bool hasConnection = false;

  //This is how we'll allow subscribing to connection changes
  StreamController connectionChangeController = StreamController();

  //flutter_connectivity
  final Connectivity _connectivity = Connectivity();

  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChange);
  }

  //flutter_connectivity's listener
  void _connectionChange(ConnectivityResult result) {
    hasInternetInternetConnection();
  }

  Stream get connectionChange => connectionChangeController.stream;

  Future<bool> hasInternetInternetConnection() async {
    bool previousConnection = hasConnection;
    bool firstCall = await IsFirstRun.isFirstCall();
    var connectivityResult = await (Connectivity().checkConnectivity());
    //Check if device is just connect with mobile network or wifi
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      //Check there is actual internet connection with a mobile network or wifi
      if (await InternetConnectionChecker().hasConnection) {
        // Network data detected & internet connection confirmed.
        hasConnection = true;
        log("$hasConnection", name: "HAS_CONNECTION");
        // if (previousConnection != hasConnection &&
        //     previousConnection == !hasConnection &&
        //     !firstCall) {
        //   Get.snackbar(
        //     'Internet detected',
        //     'You are now connected to the internet',
        //     duration: Duration(seconds: 4),
        //     colorText: Colors.green,
        //     backgroundColor: Colors.green.withOpacity(0.2),
        //   );
        // }
      } else {
        // Network data detected but no internet connection found.
        hasConnection = false;

        /*Get.snackbar(
          'No internet detected',
          'Please check your internet connectivity',
          duration: Duration(seconds: 4),
          colorText: Colors.red,
          backgroundColor: Colors.red.withOpacity(0.2),
          snackPosition: SnackPosition.BOTTOM, margin: const EdgeInsets.all(16)
        );*/
      }
    }
    // device has no mobile network and wifi connection at all
    else {
      hasConnection = false;
      log("$hasConnection", name: "HAS_CONNECTION");

      /*Get.snackbar(
        'No internet detected',
        'Please check your internet connectivity',
        duration: Duration(seconds: 4),
        colorText: Colors.red,
        backgroundColor: Colors.red.withOpacity(0.1),
        snackPosition: SnackPosition.BOTTOM, margin: const EdgeInsets.all(16)
      );*/
    }
    // The connection status changed send out an update to all listeners
    if (previousConnection != hasConnection) {
      connectionChangeController.add(hasConnection);
    }
    return hasConnection;
  }
}
