import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../controllers/login_controllers.dart';

class LoginPages extends StatefulWidget {
  const LoginPages({Key? key}) : super(key: key);

  @override
  State<LoginPages> createState() => _LoginPagesState();
}

class _LoginPagesState extends State<LoginPages> {
  UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title:Image.asset(
          "assets/logo.png",
          width: 150,
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Positioned.fill(
              child: Image.asset(
                'assets/bg2.jpg',
                repeat: ImageRepeat.repeat,
              ),
            ),
            Container(
              height: 450,
              decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.only(top:30,right:10,left:10),
              padding: EdgeInsets.only(top:30,right:20,left:20,bottom: 20),
              child: Center(
                child: Column(
                  children: [
                    Image.asset(
                      "assets/logo.png",
                    ),
                    Text(
                        "Login Sebagai Siswa".toUpperCase(),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    FormBuilder(
                      key: _formKey,
                      // enabled: false,
                      onChanged: () {
                        _formKey.currentState!.save();
                      },
                      autovalidateMode: AutovalidateMode.disabled,
                      child: Column(
                        children: <Widget>[
                          FormBuilderTextField(
                            name: 'nis',
                            decoration: InputDecoration(
                                labelText: "Masukan NIS Kamu",
                                labelStyle: TextStyle(
                                    fontSize: 14
                                )
                            ),
                            onChanged: (val) {
                              print(val); // Print the text value write into TextField
                            },
                            keyboardType: TextInputType.number,
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(),
                            ]),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          FormBuilderTextField(
                            name: 'password',
                            decoration: InputDecoration(
                                labelText: "Masukan Password Kamu",
                                labelStyle: TextStyle(
                                    fontSize: 14
                                )
                            ),
                            onChanged: (val) {
                              print(val); // Print the text value write into TextField
                            },
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(),
                            ]),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: ElevatedButton(
                                  onPressed: () async {
                                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                                      debugPrint(_formKey.currentState?.value.toString());
                                      await userController.loginUser(
                                          _formKey.currentState!.fields['nis']!.value,
                                          _formKey.currentState!.fields['password']!.value
                                      );
                                      _formKey.currentState?.reset();
                                      FocusScope.of(context).unfocus();
                                    } else {
                                      debugPrint(_formKey.currentState?.value.toString());
                                      debugPrint('validation failed');
                                    }
                                  },
                                  child:const Text(
                                    'Masuk',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                              const SizedBox(width: 20),
                              Expanded(
                                child: OutlinedButton(
                                  onPressed: () {
                                    _formKey.currentState?.reset();
                                  },
                                  // color: Theme.of(context).colorScheme.secondary,
                                  child: Text(
                                    'Reset',
                                    style: TextStyle(
                                        color: Theme.of(context).colorScheme.secondary),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],

                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
