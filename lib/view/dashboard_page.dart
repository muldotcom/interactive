import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:interactivetest/main.dart';
import 'package:interactivetest/utils/my_colors.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../controllers/login_controllers.dart';
import '../models/AbsentModel.dart';
import '../utils/connectivity_util.dart';
import '../utils/database_helper.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  UserController userController = Get.put(UserController());

  String? _currentAddress;
  Position? _currentPosition;

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(forceAndroidLocationManager: true,
        desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() => _currentPosition = position);
      _getAddressFromLatLng(_currentPosition!);
    }).catchError((e) {
      print("error ${e.toString()}");

      debugPrint(e);
    });
  }

  Future<void> _getAddressFromLatLng(Position position) async {
    await placemarkFromCoordinates(
        _currentPosition!.latitude, _currentPosition!.longitude)
        .then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      setState(() {
        _currentAddress =
        '${place.street}, ${place.subLocality}, ${place.subAdministrativeArea}, ${place.postalCode}';
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  List<Data> taskList = [];

  void _query() async {
    taskList.clear();
    await dbHelper.queryAllRows().then((value) {
      setState(() {
        value.forEach((element) {
          taskList.add(Data(
              key: element['key'],
              checkinDate: element["checkin_date"],
              checkinTime: element["checkin_time"],
              lat: element["lat"],long: element["long"]));
        });
      });
    }).catchError((error) {
      print(error);
    });
  }
  bool hasInterNetConnection = false;

  void _delete({required String columnTime}) async {
    final rowsDeleted = await dbHelper.delete(columnTime);
    debugPrint('deleted $rowsDeleted row(s): row $columnTime');
  }


  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _query();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    // //Create instance
    // ConnectionUtil connectionStatus = ConnectionUtil.getInstance();
    // //Initialize
    // connectionStatus.initialize();
    // //Listen for connection change
    // connectionStatus.connectionChange.listen(connectionChanged);


  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {

    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
          setState(() {
            hasInterNetConnection = true;
          });
          taskList.forEach((element) async {
            await userController.addAbsentSync(
                lat: _currentPosition?.latitude.toString() ?? "",
                long:  _currentPosition?.longitude.toString() ?? "",
                apikey: element.key ?? "",
                checkin_date: element.checkinDate ?? "",
                checkin_time: element.checkinTime ?? ""
            );
            _delete(columnTime: element.checkinTime ?? "");
            _query();
          });
    }
    // device has no mobile network and wifi connection at all
    else {
      setState(() {
        hasInterNetConnection = false;
      });
    }
  }


  void connectionChanged(dynamic hasConnection) {
    setState(() {
      hasInterNetConnection = hasConnection;
      //
    });
    if(hasInterNetConnection){
      taskList.forEach((element) async {
        if(_currentPosition?.latitude.toString() == "" && _currentPosition?.longitude.toString() == ""){
          _delete(columnTime: element.checkinTime ?? "");
        }
        else{
          await userController.addAbsentSync(
              lat: _currentPosition?.latitude.toString() ?? "",
              long:  _currentPosition?.longitude.toString() ?? "",
              apikey: element.key ?? "",
              checkin_date: element.checkinDate ?? "",
              checkin_time: element.checkinTime ?? ""
          );
          _delete(columnTime: element.checkinTime ?? "");
          _query();
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title:Image.asset(
          "assets/logo.png",
          width: 150,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: Get.height,
          child: Stack(
            children: [
              Positioned.fill(
                child: Image.asset(
                  'assets/bg2.jpg',
                  repeat: ImageRepeat.repeat,
                ),
              ),
              Container(
                height: 200,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage( "assets/slide.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: null /* add child content here */,
              ),
              Positioned(
                top: 160,
                left: 0,
                right: 0,
                child: Container(
                  margin: EdgeInsets.only(left:10,right: 10),
                  width: Get.width,
                  height: Get.height,

                  child: Center(
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              /*   border: Border.all(
                        color: Colors.red[500],
                      ),*/
                              borderRadius: BorderRadius.all(Radius.circular(10))
                          ),
                          padding: EdgeInsets.only(left:10,top:25,bottom:25,right:10),
                          child: Row(
                            children: [
                              Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                      color: Colors.black,
                                      width: 1
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(50))
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(50.0),
                                  child: Image.asset(
                                    "assets/avatar.jpg",
                                    width: 50,
                                    height: 50,
                                  )
                                ),
                              ),
                              SizedBox(
                                  width:10
                              ),
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Bambang Irawan Suryono",
                                      style: TextStyle(
                                        fontSize: 14
                                      ),
                                    ),
                                    SizedBox(
                                      height:5
                                    ),
                                    Text(
                                      "XII TKJ A",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                  width:10
                              ),
                                Obx(() {
                                  return InkWell(
                                    onTap: () async {
                                      final hasPermission = await _handleLocationPermission();
                                      if (!hasPermission){
                                        Get.snackbar(
                                          "Akses Lokasi",
                                          "Membutuhkan Akses Lokasi Untuk Melanjutkan Absensi",
                                          colorText: Colors.black,
                                          backgroundColor: MyColors.primaryBackground,
                                          icon: const Icon(Icons.check_circle,color: Colors.black,),
                                        );
                                        return;
                                      };
                                      userController.isAbsent.value = !userController.isAbsent.value;
                                      EasyLoading.show(status: 'Sistem sedang mencari lokasi ...');
                                      await Geolocator.getCurrentPosition(forceAndroidLocationManager: true,
                                          desiredAccuracy: LocationAccuracy.high)
                                          .then((Position position) {
                                        setState(() => _currentPosition = position);
                                        _getAddressFromLatLng(_currentPosition!);
                                      }).catchError((e) {
                                        Get.snackbar(
                                          "Gagal Absensi",
                                          "Sistem tidak dapat menemukan lokasi anda",
                                          colorText: Colors.black,
                                          backgroundColor: MyColors.primaryBackground,
                                          icon: const Icon(Icons.check_circle,color: Colors.black,),
                                        );
                                      });
                                      EasyLoading.dismiss();
                                      DateTime now = new DateTime.now();
                                      DateTime date = new DateTime(now.year, now.month, now.day);
                                      var tanggal = "${date.year}:${date.month}:${date.day}";
                                      var jam = "${now.hour.toString().padLeft(2,'0')}:${now.minute.toString().padLeft(2,'0')}:${now.second.toString().padLeft(2,'0')}";


                                      if(_currentPosition?.latitude.toString() == "" && _currentPosition?.longitude.toString() == ""){
                                        Get.snackbar(
                                          "Gagal Absensi",
                                          "Sistem tidak dapat menemukan lokasi anda",
                                          colorText: Colors.black,
                                          backgroundColor: MyColors.primaryBackground,
                                          icon: const Icon(Icons.check_circle,color: Colors.black,),
                                        );
                                        return;
                                      }
                                      if(!hasInterNetConnection){
                                        Map<String, dynamic> row = {
                                          DatabaseHelper.columnkey: 'muliarifai',
                                          DatabaseHelper.columnDate: tanggal,
                                          DatabaseHelper.columnTime: jam,
                                          DatabaseHelper.columnLat: _currentPosition?.latitude.toString() ?? "",
                                          DatabaseHelper.columnLong: _currentPosition?.longitude.toString() ?? ""
                                        };
                                        print(row);
                                        print(tanggal);
                                        print(jam);
                                        print(_currentPosition?.latitude.toString());
                                        print(_currentPosition?.longitude.toString());
                                        final id = await dbHelper.insert(row);
                                        debugPrint('inserted row id: $id');
                                        _query();
                                        return;
                                      }
                                      else{
                                        userController.addAbsent(
                                            lat: _currentPosition?.latitude.toString() ?? "",
                                            long:  _currentPosition?.longitude.toString() ?? "",
                                            apikey: 'muliarifai',
                                            checkin_date: tanggal,
                                            checkin_time: jam
                                        );
                                      }

                                      // _currentPosition?.latitude ?? ""
                                      // _currentPosition?.longitude ?? ""



                                      // userController.isAbsent.value = !userController.isAbsent.value;
                                      // DateTime now = new DateTime.now();
                                      // DateTime date = new DateTime(now.year, now.month, now.day);
                                      //
                                      // //2023-01-30
                                      // print(date.day); // 2016-01-25
                                      // print(date.month); // 2016-01-25
                                      // print(date.year);
                                      // var tanggal = "${date.year}:${date.month}:${date.day}";
                                      // print(tanggal);
                                      // //13:45:00
                                      // print(date.hour);
                                      // print(date.minute);
                                      // print(date.second);
                                      // var jam = "${date.hour}:${date.minute}:${date.second}";
                                      // print(jam);
                                      //
                                      // DateTime dateToday =new DateTime.now();
                                      // String date2 = dateToday.toString().substring(0,10);
                                      // print(date2); // 2021-06-24
                                      //
                                      //
                                      //
                                      // DateTime now2 = DateTime.now();
                                      // String convertedDateTime = "${now2.hour.toString().padLeft(2,'0')}:${now2.minute.toString().padLeft(2,'0')}:${now2.second.toString().padLeft(2,'0')}";
                                      // print(convertedDateTime);
                                    },
                                    child: Container(
                                      width: 80,
                                      height: 50,
                                     // padding: EdgeInsets.only(left:3,right: 3,bottom: 3,top: 3),
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          color: MyColors.greyTextColor,
                                          // border: Border.all(
                                          //   color: Colors.black38,
                                          //   width: 5
                                          // ),
                                          borderRadius: BorderRadius.all(Radius.circular(10))
                                      ),
                                      // child: Obx(() {
                                      //     if(userController.isLoading.isTrue){
                                      //       return Center(
                                      //         child: Container(
                                      //           width: 30,
                                      //           height: 30,
                                      //           child: CircularProgressIndicator(),
                                      //         ),
                                      //       );
                                      //     }
                                      //     else{
                                      //       return Container(
                                      //         child: Center(
                                      //           child: Column(
                                      //             crossAxisAlignment: CrossAxisAlignment.center,
                                      //             mainAxisAlignment: MainAxisAlignment.center,
                                      //             children: [
                                      //               Icon(
                                      //                 Icons.login,
                                      //                 size: 20,
                                      //                 color: Colors.white,
                                      //               ),
                                      //               Text(
                                      //                 userController.isAbsent.value ? "Pulang" : "Masuk",
                                      //                 style: TextStyle(
                                      //                     fontSize: 12,
                                      //                     color: Colors.white,
                                      //                     fontWeight: FontWeight.bold
                                      //                 ),
                                      //               )
                                      //             ],
                                      //           ),
                                      //         ),
                                      //         width: 80,
                                      //         height: 50,
                                      //         decoration: BoxDecoration(
                                      //             color: userController.isAbsent.value ? MyColors.vividRad : MyColors.actionBar,
                                      //             // border: Border.all(
                                      //             //     color: Colors.black38,
                                      //             //     width: 2
                                      //             // ),
                                      //             borderRadius: BorderRadius.all(Radius.circular(10))
                                      //         ),
                                      //       );
                                      //     }
                                      //   }
                                      // ),
                                      child:  userController.isLoading.isTrue ?  Center(
                                        child: Container(
                                          width: 30,
                                          height: 30,
                                          child: CircularProgressIndicator(),
                                        ),
                                      )  : Container(
                                        child: Center(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.login,
                                                size: 20,
                                                color: Colors.white,
                                              ),
                                              Text(
                                                "Masuk",
                                                //userController.isAbsent.value ? "Pulang" : "Masuk",
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        width: 80,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color : MyColors.vividRad,
                                            //color: userController.isAbsent.value ? MyColors.vividRad : MyColors.actionBar,
                                            // border: Border.all(
                                            //     color: Colors.black38,
                                            //     width: 2
                                            // ),
                                            borderRadius: BorderRadius.all(Radius.circular(10))
                                        ),
                                      ),

                                    ),
                                  );
                                }
                              ),
                              // Column(
                              //   mainAxisAlignment: MainAxisAlignment.center,
                              //   children: [
                              //     Text('LAT: ${_currentPosition?.latitude ?? ""}'),
                              //     Text('LNG: ${_currentPosition?.longitude ?? ""}'),
                              //     Text('ADDRESS: ${_currentAddress ?? ""}'),
                              //     const SizedBox(height: 32),
                              //     ElevatedButton(
                              //       onPressed: _getCurrentPosition,
                              //       child: const Text("Get Current Location"),
                              //     )
                              //   ],
                              // ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 350,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10))
                          ),
                          padding: EdgeInsets.only(left:10,top:10,bottom:10,right:10),
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        "Absensi Tertunda",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    Text(
                                        //"${_connectionStatus.toString()}",
                                       hasInterNetConnection ? "Online" : "Offline",
                                      style: TextStyle(
                                          color: hasInterNetConnection ? Colors.green : Colors.red,
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 1,
                                  color: Colors.black12,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 300,
                                  width: Get.width,
                                  child:
                                  taskList.isNotEmpty ? ListView.builder(
                                    itemCount: taskList.length,
                                      itemBuilder: (ctx, index) {
                                    var data = taskList[index];
                                    return Container(
                                      decoration: BoxDecoration(
                                          color: index.isEven ? MyColors.accent : MyColors.accentDark,
                                          // border: Border.all(
                                          //   color: Colors.red[500],
                                          // ),
                                          borderRadius: BorderRadius.all(Radius.circular(5))
                                      ),
                                     margin: EdgeInsets.only(bottom: 10),
                                     padding: EdgeInsets.only(left:10,right: 10,bottom: 10,top: 10),
                                     child: Row(
                                       children: [
                                         Text(
                                           data.key ?? "",
                                           style: TextStyle(
                                             color: Colors.white,
                                           ),
                                         ),
                                         SizedBox(
                                           width: 10,
                                         ),

                                         Text(
                                           data.checkinDate ?? "",
                                           style: TextStyle(
                                             color: Colors.white,
                                           ),
                                         ),
                                         SizedBox(
                                           width: 10,
                                         ),

                                         Text(
                                           data.checkinTime ?? "",
                                           style: TextStyle(
                                             color: Colors.white,
                                           ),
                                         ),
                                         SizedBox(
                                           width: 10,
                                         ),

                                         Flexible(
                                           child: Text(
                                             data.lat ?? "",
                                             overflow: TextOverflow.ellipsis,
                                             style: TextStyle(
                                               color: Colors.white,
                                             ),
                                           ),
                                         ),
                                         SizedBox(
                                           width: 10,
                                         ),
                                         Flexible(
                                           child: Text(
                                             data.long ?? "",
                                             overflow: TextOverflow.ellipsis,
                                             style: TextStyle(
                                               color: Colors.white,
                                             ),
                                           ),
                                         ),
                                         SizedBox(
                                           width: 10,
                                         ),

                                       ],
                                     ),
                                    );
                                  }) :
                                      Center(
                                        child: Container(
                                          height: 50,
                                          child: Column(
                                            children: [
                                              Text(
                                                  "Data Kosong",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold
                                                ),
                                              ),
                                              Text(
                                                "Tidak Ada absen yang tertunda"
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                ),
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
